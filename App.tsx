import React, {useEffect, useState} from 'react';
import {
  Button,
  FlatList,
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  View,
} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';

interface Todo {
  id: number;
  title: string;
  date: number;
  done: boolean;
}

function App(): JSX.Element {
  const [toDos, setTodos] = useState<Todo[]>([]);
  const [title, setTitle] = useState<string>('');

  useEffect(() => {
    const getLocalTodos = async () => {
      try {
        const localTodos = await AsyncStorage.getItem('TODOS');
        const parsedLocalTodos = JSON.parse(localTodos || '[]');
        console.log('parsedLocalTodos: ', parsedLocalTodos);
        setTodos(parsedLocalTodos);
      } catch (e) {
        console.log(e);
      }
    };

    getLocalTodos();
  }, []);

  const updateLocalTodos = async (newTodos: Todo[]) => {
    try {
      const jsonNewToDos = JSON.stringify(newTodos);
      await AsyncStorage.setItem('TODOS', jsonNewToDos);
    } catch (error) {
      console.log('error: ', error);
    }
  };

  const onAddTodoPress = () => {
    const newtoDo = {
      id: toDos.length + 1,
      title,
      date: Date.now(),
      done: false,
    };
    const newTodos = [...toDos, newtoDo];

    updateLocalTodos(newTodos);
    setTodos(newTodos);
    setTitle('');
  };

  const deleteTodo = async (id: number) => {
    try {
      const newTodos = toDos.filter(todo => todo.id !== id);

      updateLocalTodos(newTodos);
      setTodos(newTodos);
    } catch (e) {
      console.log(e);
    }
  };

  const modifyTodo = async (id: number) => {
    try {
      const newTodos = toDos.map(todo => {
        if (todo.id === id) {
          return {...todo, done: !todo.done};
        }

        return todo;
      });

      updateLocalTodos(newTodos);
      setTodos(newTodos);
    } catch (e) {
      console.log(e);
    }
  };

  const renderTodos = ({item}: any) => {
    return (
      <View style={styles.itemContainer}>
        <Text
          style={{textDecorationLine: !!item.done ? 'line-through' : 'none'}}>
          {item.title}
        </Text>

        <Button
          title={!!item.done ? 'Undo' : 'Done'}
          onPress={() => modifyTodo(item.id)}
        />
        <Button title="Delete" onPress={() => deleteTodo(item.id)} />
      </View>
    );
  };

  return (
    <SafeAreaView
      style={{
        marginHorizontal: 20,
        marginVertical: 10,
      }}>
      <TextInput
        style={{
          height: 50,
          borderWidth: 1,
          borderColor: 'black',
          borderRadius: 10,
          paddingHorizontal: 10,
        }}
        placeholder="Enter your TODO here"
        onChangeText={text => setTitle(text)}
        value={title}
      />

      <Button title="Add TODO" onPress={onAddTodoPress} />

      <FlatList
        data={toDos}
        renderItem={renderTodos}
        keyExtractor={(item, index) => index.toString()}
      />
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  mainButton: {
    flex: 1,
    backgroundColor: 'light-orange',
  },
  itemContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default App;
